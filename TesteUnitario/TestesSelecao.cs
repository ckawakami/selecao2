﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Selecao2;

namespace TesteUnitario
{
    [TestClass]
    public class TestesSelecao
    {
        [TestMethod]
        public void ValidarValorVazio()
        {
            Assert.AreEqual(-99, Selecao2.SomarNumeros.Validar(""));
        }

        [TestMethod]
        public void ValidarInteiro()
        {
            Assert.AreEqual(-66, Selecao2.SomarNumeros.Validar("teste"));
            Assert.AreEqual(-66, Selecao2.SomarNumeros.Validar("3.5"));
        }

        [TestMethod]
        public void ValidarValorNegativo()
        {
            Assert.AreEqual(-88, Selecao2.SomarNumeros.Validar("-12"));
        }

        [TestMethod]
        public void ValidarValorMaiorQueCem()
        {
            Assert.AreEqual(-77, Selecao2.SomarNumeros.Validar("110"));
        }

        [TestMethod]
        public void ValidarValorInicialMaiorValorFinal()
        {
            Assert.AreNotEqual(0, Selecao2.SomarNumeros.CalcularValor(21,44));
        }

        [TestMethod]
        public void VerificarConversaoEscrita()
        {
            Assert.AreEqual("Trinta", Selecao2.SomarNumeros.NumeroParaPalavra(30));
            Assert.AreEqual("Trinta e Dois", Selecao2.SomarNumeros.NumeroParaPalavra(32));           
        }

        [TestMethod]
        public void VerificarContagemCaracteres()
        {
            Assert.AreEqual(6, Selecao2.SomarNumeros.ContarCaracteres(Selecao2.SomarNumeros.NumeroParaPalavra(30)));
            Assert.AreEqual(13, Selecao2.SomarNumeros.ContarCaracteres(Selecao2.SomarNumeros.NumeroParaPalavra(32)));
        }

        [TestMethod]
        public void VerificarSomaCaracteres()
        {
            Assert.AreEqual(21, Selecao2.SomarNumeros.CalcularValor(1,5));
            Assert.AreEqual(44, Selecao2.SomarNumeros.CalcularValor(15,20));
        }

    }
}
