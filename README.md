Código feito para o seguinte problema apresentado:

Se os números de 1 a 5 fossem escritos em palavras: um, dois, três, quatro, cinco, então teríamos utilizado 2 + 4 + 4 + 6 + 5 = 21 letras no total.
Desenvolva uma aplicação  console (C#) que permita o input do valor inicial e final (limite de 100)  e  processe a lógica descrita acima.

A validação foi feita considerando:

1.  Que os valores de entrada não podem ser vazios;
2.  Que os valores de entrada devem ser obrigatoriamente números inteiros;
3.  Que os valores não podem ser inferiores a 0 e superiores a 100;
4.  Que o valor final não pode ser superior ao valor inicial.

A lógica está baseada nessas premissas, podendo ser alterada caso o negócio necessite (por exemplo, inverter no cálculo o valor final e inicial caso o valor final
fosse maior que o inicial, ao invés de validar ou aceitar números negativos).

Eventuais melhorias poderão ser efetuadas ao longo do desenvolvimento.

