﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Selecao2
{
    public class SomarNumeros
    {      

        public static void Main(string[] args)
        {
            int valorInicial;
            string valorDigitado;
            int? codigoErro;
            Console.WriteLine("Informe o valor inicial (mímimo 0): ");
            valorDigitado = Console.ReadLine();
            codigoErro = Validar(valorDigitado);
            if (codigoErro.HasValue)
            {
                Console.WriteLine(MensagensDeErro(codigoErro.Value));
                Console.ReadLine();
                return;
            }
            else
            {
                valorInicial = int.Parse(valorDigitado);
            }           

            int valorFinal;
            Console.WriteLine("Informe o valor final (máximo 100): ");
            valorDigitado = Console.ReadLine();
            codigoErro = Validar(valorDigitado);
            if (codigoErro.HasValue)
            {
                Console.WriteLine(MensagensDeErro(codigoErro.Value));
                Console.ReadLine();
                return;
            }
            else
            {
                valorFinal = int.Parse(valorDigitado);
            }            

            int valor = CalcularValor(valorInicial, valorFinal); 
            if(valor == 0)
            {
                Console.WriteLine("O valor final deve ser maior que o valor inicial.");
                Console.ReadLine();
                return;
            }
            else
            {
                Console.WriteLine("Resultado: " + valor);
                Console.ReadLine();
            }         

        }

        public static int CalcularValor(int valorInicial, int valorFinal)
        {
            if (valorInicial > valorFinal)
                return 0;

            int total = 0;
            for (int i= valorInicial; i<= valorFinal; i++)
            {
                total += ContarCaracteres(NumeroParaPalavra(i));
            }

            return total;
        }

        public static string NumeroParaPalavra(int numero)
        {
            if (numero == 0)
                return "Zero";
            if (numero == 100)
                return "Cem";

            string palavras = string.Empty;
            if (numero > 0 && numero < 100)
            {
                var MapaUnidade = new[] { "", "Um", "Dois", "Três", "Quatro", "Cinco", "Seis", "Sete", "Oito", "Nove", "Dez", "Onze", "Doze", "Treze", "Quatorze", "Quinze", "Dezesseis", "Dezessete", "Dezoito", "Dezenove" };
                var MapaDezena = new[] { "", "", "Vinte", "Trinta", "Quarenta", "Cinquenta", "Sessenta", "Setenta", "Oitenta", "Noventa" };

                if (numero < 20)
                    palavras += MapaUnidade[numero];
                else
                {
                    palavras += MapaDezena[numero / 10];
                    if ((numero % 10) > 0)
                        palavras += " e " + MapaUnidade[numero % 10];
                }
            }
            return palavras;
        }

        public static int ContarCaracteres(string numero)
        {
            int totalCaracteres = 0;
            totalCaracteres = numero.Length;
            return totalCaracteres;
        }

        public static int? Validar(string valor)
        {
            if (string.IsNullOrEmpty(valor))
                return -99;
            else
            {            
                bool ehNumero = int.TryParse(valor, out int numero);
                if (ehNumero)
                {
                    if (numero < 0)
                        return -88;
                    else if (numero > 100)
                        return -77;
                    else
                        return null;
                }
                else
                    return -66;
            }
        }

        public static string MensagensDeErro(int? codigo)
        {
            string msg = string.Empty;
            if(codigo.HasValue)
            {
                switch (codigo.Value)
                {
                    case -99:
                        msg = "É obrigatório digitar um valor.";
                        break;
                    case -88:
                        msg = "Somente números positivos ou 0 são aceitos.";
                        break;
                    case -77:
                        msg = "Número máximo é 100.";
                        break;
                    default:
                        msg = "Somente números inteiros são aceitos.";
                        break;
                }
            }            
            return msg;
        }

    }
}
